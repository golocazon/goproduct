package services

import (
	"github.com/mitchellh/mapstructure"
	"gitlab.com/golocazon/goproduct/api/models"
	"gitlab.com/golocazon/goproduct/internal/pkg"
	"gitlab.com/golocazon/gologger"
	api2 "gitlab.com/golocazon/gowsfwk/api"
)

type ProductService interface {
	pkg.ProductController
}

type ProductServiceClient struct {
	Client *api2.Client
}

func NewProductServiceClient(client *api2.Client) *ProductServiceClient {
	return &ProductServiceClient{
		Client: client,
	}
}

func (c *ProductServiceClient) CreateProduct(Product models.Product) (error, *models.Product) {

	req, err := c.Client.NewRequest("POST", "/api/Product", Product)
	if err != nil {
		return err, nil
	}
	var response api2.Response
	_, err = c.Client.Do(req, &response)
	var ProductResponse ProductResponse
	mapstructure.Decode(response.Content, &ProductResponse)
	return err, &ProductResponse.Product
}

func (c *ProductServiceClient) UpdateProductDetails(ProductID int64, Product models.Product) (error, *models.Product) {
	req, err := c.Client.NewRequest("PUT", "/api/Product/"+string(ProductID), Product)
	if err != nil {
		return err, nil
	}
	var response api2.Response
	_, err = c.Client.Do(req, &response)
	var ProductResponse ProductResponse
	mapstructure.Decode(response.Content, &ProductResponse)
	return err, &ProductResponse.Product
}

func (c *ProductServiceClient) GetProducts(customerID int64) (error, []*models.Product) {
	gologger.Infof("Called GetProducts with customer id %v", customerID)
	req, err := c.Client.NewRequest("GET", "/api/user/"+string(customerID)+"/Products", nil)
	if err != nil {
		return err, nil
	}
	var response api2.Response
	_, err = c.Client.Do(req, &response)
	var ProductsResponse ProductsResponse
	mapstructure.Decode(response.Content, &ProductsResponse)
	return err, ProductsResponse.Products
}

func (c *ProductServiceClient) GetProduct(ProductID int64) (error, *models.Product) {
	gologger.Infof("Called GetProduct  with Product id %v", ProductID)
	req, err := c.Client.NewRequest("GET", "/api/Product/"+string(ProductID), nil)
	if err != nil {
		return err, nil
	}
	var response api2.Response
	_, err = c.Client.Do(req, &response)
	var ProductResponse ProductResponse
	mapstructure.Decode(response.Content, &ProductResponse)
	return err, &ProductResponse.Product
}

func (c *ProductServiceClient) DeleteProduct(ProductID int64) error {
	gologger.Infof("Delete GetProducts with Product id %v", ProductID)
	req, err := c.Client.NewRequest("DELETE", "/api/Product/"+string(ProductID), nil)
	if err != nil {
		return err
	}
	var response api2.Response
	_, err = c.Client.Do(req, &response)
	return err
}

func (c *ProductServiceClient) GetProductPrice(ProductID int64) (error, float64) {
	gologger.Infof("UpdateProductItem with Product id %v", ProductID)
	req, err := c.Client.NewRequest("GET", "/api/Product/"+string(ProductID)+"/price", nil)
	if err != nil {
		return err, 0
	}
	var response api2.Response
	_, err = c.Client.Do(req, &response)
	if err != nil {
		return err, 0
	}
	var ProductResponse ProductPriceResponse
	mapstructure.Decode(response.Content, &ProductResponse)
	return nil, ProductResponse.Price
}

type ProductsResponse struct {
	api2.Response
	Products []*models.Product `json:"Products"`
}

type ProductResponse struct {
	api2.Response
	Product models.Product `json:"Product"`
}

type ProductPriceResponse struct {
	api2.Response
	Price float64
}
