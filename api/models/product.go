package models

type Product struct {
	ID         int64       `json:"id"`
	Name       *string    `json:"name"`
	MinimumQuantity	int64
	QuantityIncrement int64

}

func NewProduct(name string) *Product {
	return &Product{Name: &name, }
}

