package pkg

import (
	"gitlab.com/golocazon/goproduct/internal/dbmodels"
)

type ProductRepository interface {
	Fetch(offset int64, num int64) ([]*dbmodels.Product, error)
	GetByCustomerID(customerID int64) (error, []*dbmodels.Product)
	GetByID(ProductID int64) (error, *dbmodels.Product)
	Update(Product *dbmodels.Product) (*dbmodels.Product, error)
	Store(a *dbmodels.Product) (*dbmodels.Product, error)
	Delete(id int64) (bool, error)
}
