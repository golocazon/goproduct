package pkg

import (
	"errors"
	"gitlab.com/golocazon/goproduct/api/models"
	"gitlab.com/golocazon/goproduct/internal/dbmodels"
)

func NewDefaultProductController(ProductRepository ProductRepository) *DefaultProductController {
	return &DefaultProductController{
		ProductRepository: ProductRepository,
	}
}

type DefaultProductController struct {
	ProductRepository ProductRepository
}

func (r *DefaultProductController) CreateProduct(Product models.Product) (error, *models.Product) {
	// Create a new Product
	dbProduct := dbmodels.NewDBProduct(&Product)
	createdProduct, error := r.ProductRepository.Store(dbProduct)
	return error, dbmodels.NewAPIProduct(createdProduct)
}

func (r *DefaultProductController) UpdateProductDetails(ProductID int64, Product models.Product) (error, *models.Product) {
	err, loadedProduct := r.ProductRepository.GetByID(ProductID)
	if err != nil {
		return errors.New("product does not exist"), nil
	}
	//TODO: how to ensure the Product belongs to the user?? is it needed?
	mergeProductContent(loadedProduct, Product)
	// Create a new Product
	updatedProduct, error := r.ProductRepository.Update(loadedProduct)
	return error, dbmodels.NewAPIProduct(updatedProduct)
}

/*
Merges
*/
func mergeProductContent(origin *dbmodels.Product, delta models.Product) {

	origin.Name = delta.Name
}

func (r *DefaultProductController) GetProducts(customerID int64) (err error, Products []*models.Product) {
	err, dbProducts := r.ProductRepository.GetByCustomerID(customerID)
	Products = make([]*models.Product, len(dbProducts))
	for i, _ := range dbProducts {
		Products[i] = dbmodels.NewAPIProduct(dbProducts[i])
	}
	return
}

func (r *DefaultProductController) GetProduct(ProductID int64) (err error, loadedProduct *models.Product) {
	//err, loadedProduct =  r.ProductRepository.GetByID(ProductID)
	return
}

func (r *DefaultProductController) DeleteProduct(ProductID int64) (err error) {
	_, err = r.ProductRepository.Delete(ProductID)
	return
}
