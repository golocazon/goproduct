package pkg

import (
	"gitlab.com/golocazon/goproduct/api/models"
)

type ProductController interface {
	CreateProduct(Product models.Product) (error, *models.Product)

	UpdateProductDetails(ProductID int64, Product models.Product) (error, *models.Product)

	GetProducts(customerID int64) (error, []*models.Product)

	GetProduct(ProductID int64) (error, *models.Product)

	DeleteProduct(ProductID int64) error

}
