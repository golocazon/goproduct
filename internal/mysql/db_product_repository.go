package mysql

import (
	"errors"
	"github.com/jinzhu/gorm"
	models "gitlab.com/golocazon/goproduct/internal/dbmodels"
)

func NewDBProductRepository(db *gorm.DB) *DBProductRepository {
	return &DBProductRepository{
		DB: db,
	}
}

type DBProductRepository struct {
	DB *gorm.DB
}

func (r *DBProductRepository) Fetch(offset int64, num int64) (Products []*models.Product, err error) {
	//Products := make([]dbmodels.Product, 0)
	err = r.DB.Limit(num).Offset(offset).Find(&Products).Error
	return
}

func (r *DBProductRepository) GetByCustomerID(customerID int64) (err error, Products []*models.Product) {

	err = r.DB.Where("customer_id = ?", customerID).Find(&Products).Error
	return
}

func (r *DBProductRepository) GetByID(ProductID int64) (error, *models.Product) {
	Product := &models.Product{}
	error := r.DB.First(&Product, ProductID).Error
	if error != nil {
		return errors.New("not found"), Product
	}
	return nil, Product
}

func (r *DBProductRepository) Update(Product *models.Product) (*models.Product, error) {
	// Create a new Product
	error := r.DB.Save(&Product).Error
	return Product, error
}

func (r *DBProductRepository) Store(Product *models.Product) (*models.Product, error) {
	// Create a new Product
	error := r.DB.Create(&Product).Error
	//r.DB.Save(&Product)
	return Product, error
}

func (r *DBProductRepository) Delete(ProductID int64) (bool, error) {
	Product := &models.Product{}
	Product.ID = ProductID
	error := r.DB.Delete(&Product).Error
	if error != nil {
		return false, errors.New("not found")
	}
	return true, nil
}
