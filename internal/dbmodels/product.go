package dbmodels

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/golocazon/goproduct/api/models"
)

type Product struct {
	gorm.Model
	ID         int64       `json:"id"`
	Name       *string    `json:"name"`
	MinimumQuantity	int64  `json:"minimumQuantity"`
	QuantityIncrement int64  `json:"quantityIncrement"`
}

func NewDBProduct(product *models.Product) *Product {
	newProduct := Product{
		ID:                product.ID,
		Name:              product.Name,
		MinimumQuantity:   product.MinimumQuantity,
		QuantityIncrement: product.QuantityIncrement,
	}
	return &newProduct
}


func NewAPIProduct(product *Product) *models.Product {
	newProduct := models.Product{
		ID:         product.ID,
		Name:       product.Name,
		MinimumQuantity:   product.MinimumQuantity,
		QuantityIncrement: product.QuantityIncrement,
	}
	return &newProduct
}