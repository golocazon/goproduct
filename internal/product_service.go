package internal

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"github.com/go-chi/jwtauth"
	"gitlab.com/golocazon/goproduct/api/services"
	api2 "gitlab.com/golocazon/gowsfwk/api"

	//"github.com/go-chi/render"
	"gitlab.com/golocazon/goproduct/api/models"
	"gitlab.com/golocazon/goproduct/internal/pkg"
	"net/http"
	"strconv"
)

type ProductService interface {
	Router(r chi.Router)
}

type DefaultProductService struct {
	Controller pkg.ProductController
}

func NewProductService(controller pkg.ProductController) ProductService {
	return &DefaultProductService{
		Controller: controller,
	}
}

func (c *DefaultProductService) Router(r chi.Router) {
	r.Route("/Product", func(r chi.Router) {
		r.Get("/{id}", c.getProduct())
		r.Put("/{id}", c.updateProduct)
		//r.Post("/", c.createProduct)
	})
	r.Get("/products", c.getProducts)
	r.Post("/product", c.createProduct)
	r.Get("/user/{userId}/products", c.getProducts)
	r.Delete("/product/{id}", c.deleteProduct())

}

type ProductInput struct {
	ID    int64
	name  string
	items []*ProductItemInput
}

type ProductItemInput struct {
	name string
}

type GetProductRequest struct {
}

type ResponseError struct {
	description string
}

type Response struct {
	code  int
	error *ResponseError
}

type GetProductResponse struct {
	Response
	Product ProductInput
}

func (s *DefaultProductService) getProduct() http.HandlerFunc {
	type request struct {
		Name string
	}
	type response struct {
		Greeting string `json:"greeting"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		val := chi.URLParam(r, "id")
		ProductID, err := strconv.Atoi(val)
		if err != nil {
			http.Error(w, http.StatusText(500), 500)
			return
		}
		err, createdProduct := s.Controller.GetProduct(int64(ProductID))
		if err != nil {
			http.Error(w, http.StatusText(404), 404)
			return
		}

		js, err := json.Marshal(&services.ProductResponse{Product: *createdProduct})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
	}
}

type CreateProductRequest struct {
	Product ProductInput
}

type CreateProductResponse struct {
	Product ProductInput
}

func (c *DefaultProductService) createProduct(w http.ResponseWriter, r *http.Request) {
	var Product models.Product
	err := json.NewDecoder(r.Body).Decode(&Product)
	if err != nil {
		http.Error(w, http.StatusText(500), 500)
		return
	}
	err, createdProduct := c.Controller.CreateProduct(Product)
	if err != nil {
		http.Error(w, http.StatusText(404), 404)
		return
	}

	js, err := json.Marshal(&services.ProductResponse{Product: *createdProduct})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)

}

type UpdateProductRequest struct {
	Product ProductInput
}

type UpdateProductResponse struct {
	Product ProductInput
}

func (c *DefaultProductService) updateProduct(w http.ResponseWriter, r *http.Request) {
	val := chi.URLParam(r, "id")
	ProductID, err := strconv.Atoi(val)
	if err != nil {
		http.Error(w, http.StatusText(500), 500)
		return
	}
	var updateProduct models.Product
	err = json.NewDecoder(r.Body).Decode(&updateProduct)
	err, Product := c.Controller.UpdateProductDetails(int64(ProductID), updateProduct)
	//c.Controller.UpdateProductItem()
	err = json.NewDecoder(r.Body).Decode(&Product)
	if err != nil {
		http.Error(w, http.StatusText(500), 500)
		return
	}
	//Product.
	if err != nil {
		http.Error(w, http.StatusText(404), 404)
		return
	}
	/*
		js, err := json.Marshal(&UpdateProductResponse{Product: &ProductInput{}})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	*/
	w.Header().Set("Content-Type", "application/json")
	//w.Write(js)

}

func getUserId(r *http.Request) (userID int64, err error) {
	userIDVal := chi.URLParam(r, "userId")
	if len(userIDVal) > 0 {
		userIDAsInt, _ := strconv.Atoi(userIDVal)
		userID = int64(userIDAsInt)
	} else {
		_, claims, _ := jwtauth.FromContext(r.Context())
		userIDAsFloat64 := claims["user_id"].(float64)
		userID = int64(userIDAsFloat64)
	}
	return
}

func (c *DefaultProductService) getProducts(w http.ResponseWriter, r *http.Request) {

	userID, _ := getUserId(r)

	err, Products := c.Controller.GetProducts(userID)
	if err != nil {
		http.Error(w, http.StatusText(404), 404)
		return
	}
	//render.Status(r, http.StatusOK )
	//render.Render(w, r, &ProductsResponse{ Products: &Products,})
	js, err := json.Marshal(&api2.Response{
		Content: services.ProductsResponse{Products: Products},
		Error:   nil,
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

func (s *DefaultProductService) deleteProduct() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		val := chi.URLParam(r, "id")
		ProductID, err := strconv.Atoi(val)
		if err != nil {
			http.Error(w, http.StatusText(500), 500)
			return
		}
		err = s.Controller.DeleteProduct(int64(ProductID))
		if err != nil {
			http.Error(w, http.StatusText(404), 404)
			return
		}

		js, err := json.Marshal(&services.ProductResponse{})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
	}
}
