module gitlab.com/golocazon/goproduct

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-chi/cors v1.0.0
	github.com/go-chi/jwtauth v4.0.3+incompatible
	github.com/go-chi/render v1.0.1
	github.com/jinzhu/gorm v1.9.11
	github.com/spf13/viper v1.5.0
	gitlab.com/golocazon/gologger v0.0.3
	gitlab.com/golocazon/gowsfwk v0.0.1
)
