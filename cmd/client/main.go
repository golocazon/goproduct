package main

import (
	"github.com/spf13/viper"
	"gitlab.com/golocazon/goproduct/api/services"
	logger "gitlab.com/golocazon/gologger"
	api2 "gitlab.com/golocazon/gowsfwk/api"
	"log"
	"net/url"
)

var (
	DefaultLogger logger.Logger
)

func configLogging() {
	config := logger.Configuration{
		EnableConsole:     true,
		ConsoleLevel:      logger.Debug,
		ConsoleJSONFormat: true,
		EnableFile:        true,
		FileLevel:         logger.Info,
		FileJSONFormat:    true,
		FileLocation:      "log.log",
	}

	err := logger.NewLogger(config, logger.InstanceZapLogger)
	if err != nil {
		log.Fatalf("Could not instantiate log %s", err.Error())
	}

	contextLogger := logger.Log.WithFields(logger.Fields{"key1": "value1"})
	contextLogger.Debugf("Starting with zap")
	contextLogger.Infof("Zap is awesome")

	err = logger.NewLogger(config, logger.InstanceLogrusLogger)
	if err != nil {
		log.Fatalf("Could not instantiate log %s", err.Error())
	}
	contextLogger = logger.WithFields(logger.Fields{"key1": "value1"})
	contextLogger.Debugf("Starting with logrus")

	contextLogger.Infof("Logrus is awesome")
	DefaultLogger = logger.WithFields(logger.Fields{"Application": "GoCartClient"})
}

func configConfiguration() {
	viper.SetConfigName("gocart_config.json") // name of config file (without extension)
	viper.AddConfigPath("$HOME/.goproduct")      // call multiple times to add many search paths
	viper.AddConfigPath(".")                  // optionally look for config in the working directory

	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		//		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}
}

func main() {

	configConfiguration()
	configLogging()
	secretString := viper.GetString("secret")
	client := api2.NewClient(nil, &url.URL{
		Host:   "localhost:1324",
		Scheme: "http",
	}, "Eric UA", secretString)

	productService := services.NewProductServiceClient(
		client)
	err, products := productService.GetProducts(3)
	if err != nil {
		logger.Infof("Error calling GetProducts %v", err)
		panic(err)
	}
	if len(products) == 0 {
		logger.Infof("No product found")
	} else {
		for _, cart := range products {
			logger.Infof("Got product %v", *cart)
		}
	}
}
